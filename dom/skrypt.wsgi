#!/usr/bin/env python


from cgi import parse_qs, escape
import bookdb

titlesHtml = []
books = bookdb.BookDB()
for title in books.titles():
    titlesHtml.append("<li>"+"<a href=""http://194.29.175.240/~p5/wsgi/?id="+title["id"]+"&"">"+title['title']+"</a>"+"</li>")

body = """
<html>
<body>
         Lista ksiazek:
            %s
</body>
</html>"""

html = """
<html>
<body>
    <p>Tytul: %s</p>
    <p>ISBN: %s</p>
    <p>Wydawca: %s</p>
    <p>Autor: %s</p>
    <a href="http://194.29.175.240/~p5/wsgi">Lista ksiazek</a>
</body>
</html>
"""

def application(environ, start_response):

    d = parse_qs(environ['QUERY_STRING'])

    idBook = d.get('id', [''])[0]

    idBook = escape(idBook)
    if idBook is '':
        response_body = body % ( "".join(titlesHtml))
    else:
        response_body = html % (books.title_info(idBook)['title'],
                                books.title_info(idBook)['isbn'],
                                books.title_info(idBook)['publisher'],
                                books.title_info(idBook)['author'])


    status = '200 OK'


    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    s = make_server('0.0.0.0', 6969, application)
    s.serve_forever()
